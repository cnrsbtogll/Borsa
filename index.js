/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './Finance';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
